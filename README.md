# Welcome!

This project contains all the public related files surrounding the Acuitas Cryptocurrency Trading Platform VIP Launch.

## What is the VIP Launch?

The VIP Launch is the presale of licenses for the Acuitas Cryptocurrency Trading Platform. It is a one of a kind license that provides features, discounts, and access 
to the Acuitas Trading Tecnoliges team and yet to be released products.

## What is the Acuitas Cryptocurrency Trading Platform?

The Acuitas Cryptocurrency Trading Platform is a next generation cryptocurrency trading platform. It integrates an interactive dashboard, a community driven marketplace 
for buying, selling, and leasing of proven strategies, an advanced advisor and backtesting engines, along with the proprietary Visual Strategy Workspace to create a 
seamless and powerful trading experience.

You can learn more about the above at https://www.acuitas.app

## The Blackpaper

Yup, we have a whitepaper, except it's black. So, it's really a blackpaper. Feel free to give it a read and pass it around.